
import CalGrade from "../components/CalGrade";
import {useState} from "react";

import Button from '@mui/material/Button';
import Grid from '@mui/material/Grid';
import { Typography, Box, Container} from '@mui/material';

function CalGradePage() {

    const [name, setName] = useState("Merie_Lrote");
    const [score, setScore] = useState("0");
    const [translateResult, setTranslateResult] = useState("");

    
    
    function Calculate() {
        let x = parseFloat(score);
        setScore(x);
        if (x < 49 ) {
            setTranslateResult("E");
        }else if (x < 55 ){
            setTranslateResult("D");
        }else if (x < 60 ){
            setTranslateResult("D+");
        }else if (x < 65 ){
            setTranslateResult("C");
        }else if (x < 70 ){
            setTranslateResult("C+");
        }else if (x < 75 ){
            setTranslateResult("B");
        }else if (x < 80 ){
            setTranslateResult("B+");
        }else {
            setTranslateResult("A");
        }
    }

    return (

        <Container maxWidth='lg'>
            <Grid container spacing={2} sx={{marginTop : "10px"}}>
            <Grid item xs={12}>
                <Typography variant="h5">
                เว็บตัดเกรดวิชา 344-2xx
                </Typography>
            </Grid>
            <Grid item xs={8}>
                <Box sx={{textAlign : "center"}}>

                Name: <input type="text"
                            value={name}
                            onChange={ (e) => {setName(e.target.value); }}
                            /> <br />
                            <br />
                score: <input type="text"
                            value={score}
                            onChange={ (e) => {setScore(e.target.value); }} 
                            /> <br />
                            <br />
                
                <Button variant="contained" onClick={ () =>{Calculate()}} >CalGrade </Button>
                </Box>
            </Grid>
            <Grid item xs={4}>
                { score != 0 &&
                    <div>
                        <hr />

                        ผลการคำนวณ
                        <CalGrade
                            name={name}
                            score={score}
                            result={translateResult}
                            />

                    </div>
                }
           </Grid>
            </Grid>
            </Container>
    );
}

export default CalGradePage;