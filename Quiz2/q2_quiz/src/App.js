import logo from './logo.svg';
import './App.css';

import AboutUsPage from './pages/AboutUsPage';
import CalGradePage from './pages/CalGradePage';
import Header from './components/Header';

import {Routes, Route} from "react-router-dom"; 


function App() {
  return (
    <div className="App">
      <Header />
      <Routes>
        <Route path="about" element={
                        <AboutUsPage />
                          } />
        

        <Route path="/" element={
                          <CalGradePage />
                          } />
      </Routes>
    </div>
  );
}

export default App;
